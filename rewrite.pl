#!/usr/bin/perl
use strict;
use FileHandle;
my $id = 1;
my $tag_txt = "";
my %watch = ( short => "yfrm", long => "yfrm", user => "user", local => "ylm", addr => "yfrm" );
my %val = ( short => "short_name", long => "yfrm", user => "user", local => "ylm", addr => "long_name" );
my $fd = new FileHandle;
$/ = undef;
open($fd,"index.html") or die;
my $c = <$fd>;
close($fd);
$c =~ s/<span id='([a-z]+)(\d+)'><\/span>/respan($1,$2)/ge;
$c =~ s/--start--.*--end--/"--start--\n".$tag_txt."\/\/--end--"/gse;
open($fd,">edit.html") or die;
print $fd $c;
close($fd);

sub respan
{
  my $key = shift;
  my $old_id = shift;
  my $newtxt = "<span id='${key}${id}'></span>";
  my $watch = $watch{$key};
  my $val   = $val{$key};
  $tag_txt .= "  tag('#${key}${id}','#${watch}','#${val}'); // --auto--\n";
  $id++;
  return $newtxt;
}
