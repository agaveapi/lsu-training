#!/bin/bash
echo 'Executing Draw Gau Code'
# Setting the x flag will echo every
# command onto stderr. This is
# for debugging, so we can see what's
# going on.
set -x
echo ==ENV=============
# The env command prints out the
# entire execution environment. This
# is also present for debugging purposes.
env
echo ==PWD=============
# We also print out the execution
# directory. Again, for debugging purposes.
pwd
echo ==JOB=============
CCTK_HOME=/work/hpctrn15
# ${parfile} will be substituted by Agave
# I like having my environment variables in
# upper case, that's the only reason for this
# assignment.
PARFILE=${parfile}
if [ "${PARFILE}" = "" ]
then
  echo "The parfile was not set"
  exit 1
fi
if [ "${PBS_NODEFILE}" = "" ]
then
  # When running on a system managed by Torque
  # this variable should be set. If it's not,
  # that's a problem.
  echo "The PBS_NODEFILE was not set"
  exit 2
fi
# Strip off the .par from the end of the parfile name.
LOGDIR=$(echo ${PARFILE} | perl -p -e 's/\.par$//')
# Create a directory with the name of the parfile.
mkdir -p ${LOGDIR}

# More debugging.
echo LOGDIR=$LOGDIR
echo PARFILE=$PARFILE
echo NODEFILE=$PBS_NODEFILE

# By default, the PBS_NODEFILE lists nodes multiple
# times, once for each MPI process that should run
# there. We only want one MPI process per node, so
# we create a new file with "sort -u".
LOCAL_NODEFILE=${LOGDIR}/nodefile.txt
sort -u < ${PBS_NODEFILE} > ${LOCAL_NODEFILE}
PROCS=$(wc -l < ${LOCAL_NODEFILE})

# Execute our MPI command.
mpirun -np ${PROCS} -machinefile ${LOCAL_NODEFILE} ${CCTK_HOME}/exe/drawgau ${PARFILE} 
