# README #

### What is this repository for? ###

* Quick summary
   To provide a place for Agave Docs created by LSU
* Version
   0.00001
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I update? ###

* Span's need to have unique id's are used to identify
  customized parameters that are used repeatedly in the
  document, e.g. user name, machine name. These are written
  with span's that have id's such as "short1" for the
  short version of a machine's name (e.g. "shelob" instead
  of "shelob.hpc.lsu.edu"). Because it would be tedious to
  keep track of whether a given id number has already been
  used, and to add the javascript to automatically update it,
  these steps are automatically handled by the rewrite.pl
  perl script. Simply insert a span with any number,
  e.g. <span id='short1'></short>, and the perl script will
  generate a new version of the file (edit.html) with a
  unique number and the relevant javascript code.

### Who do I talk to? ###

* Steven R. Brandt (sbrandt@cct.lsu.edu)
